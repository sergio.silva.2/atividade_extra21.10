#!/bin/bash


for distro in $(traf.txt | cut -d '' -f3 | uniq); do
	soma=0
	while read line; do
		distroAt=$(echo $line | cut -d '' -f3)
		traf=$(echo $line | cut -d '' -f2)

		if [ $distro == $distroAt ]; then
			soma=$((soma + $traf))
		fi
		done < traf.txt
		 echo "$distro $soma"
done
